#include <string>
#include "alt.h"

//A function to set the case of a given letter
char setCase(char letter, bool upper)
{
    if(upper)
        return std::toupper(letter);
    else
        return std::tolower(letter);
}


//The Alternating Caps function
std::string alt(std::string input)
{
    for (size_t i = 0; i < input.length(); i++)
    {
        input[i] = setCase(input[i], i%2);
    }

    return input;
}

